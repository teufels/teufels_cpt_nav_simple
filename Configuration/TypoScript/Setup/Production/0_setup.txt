#############################
## NAVIGATION SIMPLE SETUP ##
#############################

lib.navigation.simple = COA
lib.navigation.simple {

    5 = LOAD_REGISTER
    5  {
        entryLevel.cObject = TEXT
        entryLevel.cObject {
            field = entryLevel
            ifEmpty = {$plugin.tx_teufels_cpt_nav_simple.settings.lib.navigation.entryLevel}
        }
        classNav.cObject = TEXT
        classNav.cObject {
            field = classNav
            ifEmpty = {$plugin.tx_teufels_cpt_nav_simple.settings.lib.navigation.class.nav}
        }
        roleNav.cObject = TEXT
        roleNav.cObject {
            field = roleNav
            ifEmpty = {$plugin.tx_teufels_cpt_nav_simple.settings.lib.navigation.role.nav}
        }
        classDiv.cObject = TEXT
        classDiv.cObject {
            field = classDiv
            ifEmpty = {$plugin.tx_teufels_cpt_nav_simple.settings.lib.navigation.class.div}
        }
        classUl.cObject = TEXT
        classUl.cObject {
            field = classUl
            ifEmpty = {$plugin.tx_teufels_cpt_nav_simple.settings.lib.navigation.class.ul}
        }
        classLi.cObject = TEXT
        classLi.cObject {
            field = classLi
            ifEmpty = {$plugin.tx_teufels_cpt_nav_simple.settings.lib.navigation.class.li}
        }
        classA.cObject = TEXT
        classA.cObject {
            field = classA
            ifEmpty = {$plugin.tx_teufels_cpt_nav_simple.settings.lib.navigation.class.a}
        }
        cachePostfix.cObject = TEXT
        cachePostfix.cObject {
            field = cachePostfix
            ifEmpty = {$plugin.tx_teufels_cpt_nav_simple.settings.lib.navigation.cache.postfix}
        }
    }

	10 = HMENU
	10 {
	    entryLevel = TEXT
	    entryLevel.data = register:entryLevel
		1 = TMENU
		1 {
			stdWrap.dataWrap = <nav class="{register:classNav}" role="{register:roleNav}"><div class="{register:classDiv}"><ul class="{register:classUl}">|</ul></div></nav>
			expAll = 0
			noBlur = 1
			NO = 1
			NO {
				ATagTitle.field = abstract // description // title
				ATagBeforeWrap = 1
				linkWrap = |<mark class="bar"></mark>
				ATagParams.insertData = 1
				ATagParams = class="{register:classA}"
				allWrap.insertData = 1
				allWrap = |*| <li class="a {register:classLi}"> | </li> || <li class="b {register:classLi}"> | </li> || <li class="c {register:classLi}"> | </li> |*|
			}
			ACT < .NO
			ACT {
                ATagTitle.field = abstract // description // title
                ATagBeforeWrap = 1
                linkWrap = |<mark class="bar"></mark>
                ATagParams.insertData = 1
                ATagParams = class="active {register:classA}"
			}
			CUR < .ACT
		}
	}

}

# Caching for Menu
[globalVar = LIT:1 = {$plugin.tx_teufels_cpt_nav_simple.settings.production.optional.active}]
lib.navigation.simple {
    stdWrap {
        cache {
            /**
             * Hier wird die aktuelle Sprache benutzt, um einen Inhalt pro Sprache
             * für alle Seiten zu cachen.
             */
            key = tx_teufels_cpt_nav_simple_{register:cachePostfix}_{page:uid}_{TSFE:sys_language_uid}
            key.insertData = 1

            // Mit den selbstgewählten Tags kann der Cache später gezielt geleert werden.
            tags = tx_teufels_cpt_nav_simple_{register:cachePostfix}_{page:uid}_{TSFE:sys_language_uid}
            tags.insertData = 1

            /**
             * Wenn eine neue Seite generiert wird, ist das Kontaktmenü max. eine Stunde alt.
             * Wenn die Seite alle 24 Stunden neu generiert wird, könnte das Kontaktmenü
             * also auch über einen Tag alt sein!
             */
            # 3600 x 24 x 30
            lifetime = 2592000
        }
    }

    4 = TEXT
    4 {
        data = date: dmyHis
        wrap = <!--CACHED_|-->
    }
}
[global]
